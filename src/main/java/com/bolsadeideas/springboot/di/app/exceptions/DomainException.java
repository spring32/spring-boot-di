package com.bolsadeideas.springboot.di.app.exceptions;

public class DomainException extends Exception {

    public DomainException() {
    }

    public DomainException(String s) {
        super(s);
    }

    public DomainException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DomainException(Throwable throwable) {
        super(throwable);
    }
}
