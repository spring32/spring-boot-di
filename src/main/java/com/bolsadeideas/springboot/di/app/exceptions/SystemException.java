package com.bolsadeideas.springboot.di.app.exceptions;

public class SystemException extends Exception {

    public SystemException() {
    }

    public SystemException(String s) {
        super(s);
    }

    public SystemException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SystemException(Throwable throwable) {
        super(throwable);
    }
}
