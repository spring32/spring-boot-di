package com.bolsadeideas.springboot.di.app.interceptors;

import com.bolsadeideas.springboot.di.app.exceptions.DomainException;
import com.bolsadeideas.springboot.di.app.exceptions.SystemException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.sql.SQLException;

/**
 * Custom Aspect to b used by spring AOP
 * for the exception logging.
 *
 * @author rodrern
 */
@Service
@Aspect
public class ExceptionLoggingInterceptor {

    private Logger logger = LogManager.getLogger(ExceptionLoggingInterceptor.class);

    @AfterThrowing(
            pointcut = "@annotation(com.bolsadeideas.springboot.di.app.annotations.ExceptionLogger)",
            throwing = "ex")
    public void processException(JoinPoint joinPoint, Throwable ex) throws SystemException, SQLException,
            DomainException {
        if (ex instanceof SystemException) {
            // System exceptions were logged at source
            // Do not the exception, just the return
            logReturn(joinPoint, getLogger(joinPoint));
            throw (SystemException) ex;
        }
        else if (ex instanceof SQLException) {
            logException(joinPoint, ex);
            throw (SQLException) ex;
        }
        else {
            logException(joinPoint, ex);
            throw new DomainException(ex);
        }
    }

    private void logException(JoinPoint joinpoint, Throwable ex) {
        Logger log = getLogger(joinpoint);
        log.error(ex.getMessage(), ex);
        logReturn(joinpoint, log);
    }

    private void logReturn(JoinPoint joinPoint, Logger log) {
        if (log.isDebugEnabled()) {
            Method method = getMethod(joinPoint);
            log.debug("Completed " + method.getName());
        }
    }

    /**
     * Get the method that this join point surrounds.
     * Used for logging the entry into a method.
     *
     * @param jp
     * @return
     */
    protected Method getMethod(JoinPoint jp) {
        Method invoked = null;
        try {
            MethodSignature met = (MethodSignature) jp.getSignature();
            invoked = jp.getSourceLocation().getWithinType().getMethod(
                    met.getMethod().getName(),
                    met.getMethod().getParameterTypes());
        } catch(NoSuchMethodException e) {
            logger.error("Unable to get the method for logging");
            // squash it here instead of letting it bubble up.
        }
        return invoked;
    }

    /**
     * Get a logger in the context of the class instead of the
     * context of the interceptor. Otherwise every log message
     * will look like it's coming from here.
     *
     * @param joinPoint
     * @return
     */
    protected Logger getLogger(JoinPoint joinPoint) {
        return LogManager.getLogger(joinPoint.getTarget().getClass());
    }
}
